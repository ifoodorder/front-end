variable "ci_user" {
    type = string
}
variable "ci_pass" {
    type = string
}
variable "image" {
    type = string
}
job "frontend" {
    datacenters = ["dc1"]
    type = "service"
    group "frontend" {
        network {
            port "http" {
                to = 443
            }
        }
        service {
            name = "frontend"
            port = "http"
        }
        task "frontend" {
			vault {
				policies = ["frontend"]
			}
			template {
				data = file("nomad/cert.tpl")
				destination = "local/cert.pem"
			}
			template {
				data = file("nomad/key.tpl")
				destination = "secrets/key.pem"
			}
            template {
                data = file("nomad/environment.tpl")
                destination = "/usr/share/nginx/html/front-end/assets/environment/config.json"
            }
            driver = "docker"
            config {
                image = var.image
                ports = ["http"]
                auth {
                    username = var.ci_user
                    password = var.ci_pass
                }
            }
            env {
                CERT_PATH = "/local/cert.pem"
                CERT_KEY_PATH = "/secrets/key.pem"
            }
        }
    }
}