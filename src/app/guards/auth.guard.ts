import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard extends KeycloakAuthGuard implements CanActivate {
  constructor(protected router: Router,
              protected keycloakAngular: KeycloakService,
              protected logger: NGXLogger) {
    super(router, keycloakAngular);
  }
  private static getRedirectUri(state: RouterStateSnapshot): string {
    const port = location.port ? ':' + location.port : '';
    const currentUrl = location.protocol + '//' + location.hostname + port + state.url;
    history.pushState(null, document.title, currentUrl);
    return currentUrl;
  }

  private static getRoles(keycloakService: KeycloakService, route: ActivatedRouteSnapshot, logger: NGXLogger): string[] {
    const rawRouteRoles = route?.data?.roles;
    const routeRoles = (rawRouteRoles == null ? [] : rawRouteRoles as string[]);
    logger.debug(routeRoles, keycloakService.getUserRoles());
    return routeRoles;
  }

  private static hasAllRequiredRoles(requiredRoles: string[], roles: string[]): boolean {
    return requiredRoles.every(role => roles.indexOf(role) > -1);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    return new Promise((resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login({
          redirectUri: AuthGuard.getRedirectUri(state),
        }).catch(this.logger.error);
        return reject(false);
      }

      const requiredRoles: string[] = AuthGuard.getRoles(this.keycloakAngular, route, this.logger);
      const areRolesRequired = requiredRoles && requiredRoles.length !== 0;
      const hasRoles = this.roles && this.roles.length !== 0;
      if (!areRolesRequired) {
        return resolve(true);
      } else {
        if (!hasRoles) {
          return resolve(false);
        }
        return resolve(AuthGuard.hasAllRequiredRoles(requiredRoles, this.roles));
      }
    });
  }
}
