import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { NGXLogger } from 'ngx-logger';
import { forkJoin, from, Observable, of } from 'rxjs';
import { map, mergeMap, shareReplay, tap } from 'rxjs/operators';
import { EnvironmentService } from '../environment.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private keycloak: KeycloakService,
              private http: HttpClient,
              private logger: NGXLogger,
              private environment: EnvironmentService) { }

  private $isLoggedIn: Observable<boolean> = from(this.keycloak.isLoggedIn()).pipe(
    tap(x => this.logger.debug('User ' + (x ? 'is' : 'is not') + ' logged in!')),
    shareReplay(10)
  );

  public isLoggedIn(): Observable<boolean> {
    return this.$isLoggedIn;
  }

  public getUserId(): Observable<string> {
    return of(this.keycloak.getKeycloakInstance().subject);
  }

  addRole(roleName): Observable<void> {
    return forkJoin([
      this.environment.config,
      this.getUserId()
    ]).pipe(
      map(x => ({host: x[0].userHost, userId: x[1]})),
      mergeMap(x => this.http.put<void>(x.host + x.userId + '/roles/' + roleName, null)),
      tap(x => this.logger.debug('addRole', x))
    );
  }

  createGroup(): Observable<void> {
    return forkJoin([
      this.environment.config,
      this.getUserId()
    ]).pipe(
      map(x => ({host: x[0].userHost, userId: x[1]})),
      mergeMap(x => this.http.put<void>(x.host + x.userId + '/groups', null)),
      tap(x => this.logger.debug('createGroup', x))
    );
  }
  public refreshToken(): Observable<Promise<boolean>> {
    return of(this.keycloak.updateToken(-1));
  }

  register(redirectUri?: string): void {
    const defaultRedirectUri = location.protocol
      + '//'
      + location.hostname
      + (location.port ? ':' + location.port : '')
      + location.pathname;
    const $redirectUri = redirectUri || defaultRedirectUri;
    window.history.pushState(window.history.state, document.title, document.location.href);
    this.keycloak.register({
        scope: '',
        redirectUri: $redirectUri
      }).catch(e => console.error(e));

  }
}

export enum KeycloakUserRoles {
  RESTAURANT_ADMIN = 'restaurant-admin'
}