import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { EnvironmentService } from '../environment.service';
import { Restaurant, RestaurantFilter } from './restaurantApi';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private http: HttpClient,
              private logger: NGXLogger,
              private environment: EnvironmentService) { }

  postRestaurant(restaurant: Restaurant): Observable<Restaurant> {
    this.logger.debug('Post Restaurant', restaurant);
    return this.environment.config.pipe(
      map(x => x.restaurantHost),
      mergeMap(x => this.http.post<Restaurant>(x, restaurant))
    );
  }

  getRestaurantById(restaurantId: string): Observable<Restaurant> {
    return this.environment.config.pipe(
      map(x => x.restaurantHost),
      mergeMap(x => this.http.get<Restaurant>(x + restaurantId))
    );
  }

  getRestaurants(filter?: RestaurantFilter): Observable<Restaurant[]> {
    filter = filter || {};
    const urlParams = new URLSearchParams(Object.entries(filter)).toString();
    return this.environment.config.pipe(
      map(x => x.restaurantHost),
      mergeMap(x => this.http.get<Restaurant[]>([x, urlParams].join('?')))
    );
  }

  getCountries(): Observable<string[]> {
    return this.environment.config.pipe(
      map(x => x.restaurantHost),
      mergeMap(x => this.http.get<string[]>(x + 'countries'))
    );
  }
}
