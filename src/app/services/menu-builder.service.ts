import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { Menu } from '../company/menu/menu.component';
import { EnvironmentService } from '../environment.service';
import { MenuService } from './menu.service';

@Injectable({
  providedIn: 'root'
})
export class MenuBuilderService {

  constructor(private menuService: MenuService,
              private http: HttpClient,
              private logger: NGXLogger,
              private environment: EnvironmentService) { }

  public getPublicMenu(restaurantId: string): Observable<Menu> {
    return this.environment.config.pipe(
      map(x => x.menuHost),
      mergeMap(x => this.http.get<Menu>(x + restaurantId)),
      mergeMap(x => {
        if (x != null) {
          return of(x);
        }
        this.logger.debug('No menu to edit. Serving a default one');
        return this.http.get<Menu>('/assets/defaults/menu.json');
      })
    );
  }

  public getAlergens(restaurantId: string): Observable<string[]> {
    return this.environment.config.pipe(
      map(x => x.menuHost),
      mergeMap(x => this.http.get<string[]>(x + restaurantId + '/alergens'))
    )
  }

  public saveMenu(menu: Menu, restaurantId: string): Observable<{success: boolean}> {
    return this.menuService.saveMenu(menu, restaurantId)
    .pipe(
      tap(x => this.logger.debug(x)),
      map(() => ({success: true} as any)),
      catchError((e) => {
        this.logger.error(e);
        return of({success: false} as any);
      })
    );
  }
}
