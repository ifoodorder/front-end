import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private $isOpened = true;
  private blacklistedPaths = [
    '/',
    '/dev',
    '/company',
    '/company/login',
    '/company/registration'
  ];
  private restaurantItems: ListItem[] = [
    { link: '/menu', display: 'COMPANY.RESTAURANT.MENU.MENU'},
    { link: '/orders', display: 'COMPANY.RESTAURANT.MENU.ORDERS' },
    { link: '/settings', display: 'COMPANY.RESTAURANT.MENU.SETTINGS' }
  ];

  private companyItems: ListItem[] = [
    // { link: 'orders', display: 'COMPANY.MENU.ORDERS' },
    // { link: 'settings', display: 'COMPANY.MENU.SETTINGS' }
  ];

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private logger: NGXLogger) { }
  private isRestaurant(): boolean {
    let id: string = null;

    const findId = (x: ActivatedRouteSnapshot[]) => {
      for (const child of x) {
        if (child.params?.id != null) {
          id = child.params.id;
          break;
        }
        findId(child.children);
      }
    };
    findId(this.activatedRoute.snapshot.children);
    return id != null;
  }

  public isVisible(): boolean {
    const strippedPath = this.router.url.split(/\?|#/)[0];
    return !this.blacklistedPaths.includes(strippedPath);
  }

  public getList(): ListItem[] {
    const originalUrl = this.router.url;
    const path = originalUrl.split('/');
    path.pop();
    const url = path.join('/');
    if (this.isRestaurant()) {
      return this.restaurantItems.map(item => {
        return {
          link: url + item.link,
          display: item.display
        };
      });
    }
    return this.companyItems;
  }
}

export interface ListItem {
  link: string;
  display: string;
}