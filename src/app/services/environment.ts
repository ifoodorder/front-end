export interface IEnvironment {
    readonly production: boolean;
    readonly menuHost: string;
    readonly orderHost: string;
    readonly restaurantHost: string;
    readonly userHost: string;
    readonly ssoHost: string;
    readonly iconRedirectUrl: string;
    readonly mapProviderHost: string;
    readonly mapProviderKey: string;
}