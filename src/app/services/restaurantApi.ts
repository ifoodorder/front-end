export interface RestaurantFilter {
  owningOnly?: boolean;
}
export interface Restaurant {
    id?: string;
    name?: string;
    address?: Address;
    note?: string;
    logo?: string;
    visible?: boolean;
    currency?: string;
    deliverySettings?: DeliverySettings;
    paymentSettings?: PaymentSettings;
    openingHoursSettings?: OpeningHoursSettings;
  }
export interface Address {
    country?: string;
    city?: string;
    zip?: string;
    street?: string;
    building?: string;
  }
export interface DeliverySettings {
    id?: string;
    restaurantDelivery?: RestaurantDelivery;
    courierDelivery?: CourierDelivery;
    pickupPointDelivery?: PickupPointDelivery;
  }
export interface RestaurantDelivery {
    id?: string;
    enabled?: boolean;
    note?: string;
    fess?: Fee[];
  }
export interface CourierDelivery {
    id?: string;
    enabled?: boolean;
    note?: string;
    fess?: Fee[];
    area?: Point[];
    maxDistance?: string;
  }
export interface PickupPointDelivery {
    id?: string;
    enabled?: boolean;
    note?: string;
    pickupPoints?: PickupPoint[];
    fess?: Fee[];
  }
export interface PickupPoint {
    id?: string;
    name?: string;
    address?: Address;
  }
export interface Point {
    x?: string;
    y?: string;
  }
export interface Fee {
    id?: string;
    feeType?: string;
    formula?: string;
    enabled?: boolean;
  }
export interface PaymentSettings {
    id?: string;
    cash?: CashSettings;
    bankTransfer?: BankTransferSettings;
    cardOnDelivery?: CardOnDeliverySettings;
    cardOnline?: CardOnlineSettings;
    bitcoin?: BitcoinSettings;
    mealVoucher?: MealVoucherSettings;
  }
export interface CashSettings {
    id?: string;
    enabled?: boolean;
    allowUnregistered?: boolean;
    currencies?: string[];
  }
export interface BankTransferSettings {
    id?: string;
    bankAccountNumber?: string;
    additionalInfo?: string;
    allowUnregistered?: boolean;
    enabled?: boolean;
  }
export interface CardOnDeliverySettings {
    id?: string;
    enabled?: boolean;
    allowUnregistered?: boolean;
    cards?: string[];
  }
export interface CardOnlineSettings {
    id?: string;
    enabled?: boolean;
  }
export interface BitcoinSettings {
    id?: string;
    enabled?: boolean;
    address?: string;
  }
export interface MealVoucherSettings {
    id?: string;
    enabled?: boolean;
  }
export interface OpeningHoursSettings {
    id?: string;
    note?: string;
    openingHours?: OpeningHoursDaySettings[];
  }
export interface OpeningHoursDaySettings {
    id?: string;
    enabled?: boolean;
    from?: string;
    to?: string;
    day?: string;
  }
