import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { Menu, MenuItem } from '../company/menu/menu.component';
import { EnvironmentService } from '../environment.service';
import { Restaurant } from './restaurantApi';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient,
              private logger: NGXLogger,
              private environment: EnvironmentService) {
  }


  public getRestaurantMenu(restaurantId: string): Observable<Menu> {
    return this.environment.config.pipe(
      map(x => x.menuHost),
      mergeMap( x => this.http.get<Menu>(x + restaurantId)),
      tap(x => this.logger.debug(x))
    );
  }

  public getMenu(restaurantObs: Observable<Restaurant>): Observable<Menu> {
    return restaurantObs.pipe(mergeMap(restaurant => {
      return this.getRestaurantMenu(restaurant.id);
    }));
  }

  public saveMenu(menu: Menu, restaurantId: string): Observable<boolean> {
    this.logger.debug(menu);
    return this.environment.config.pipe(
      map(x => x.menuHost),
      mergeMap(x => this.http.post<any>(x + restaurantId, menu))
    );
  }

  public getMenuItem(restaurantId: string, menuItemId: string): Observable<MenuItem> {
    return this.environment.config.pipe(
      map(x => x.menuHost),
      mergeMap(x => this.http.get<MenuItem>(x + restaurantId + '/items/' + menuItemId))
    );
  }
}
