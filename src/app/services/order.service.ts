import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@swimlane/ngx-datatable';
import { NGXLogger } from 'ngx-logger';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { EnvironmentService } from '../environment.service';
import { Order } from './order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient,
              private logger: NGXLogger,
              private environment: EnvironmentService) { }

  public findOrdersLimitAndOrderBy(pageContext: PageContext)
    : Observable<DataPage<Order>> {
      return this.environment.config.pipe(
        map(x => x.orderHost),
        mergeMap(x => this.http.get<DataPage<Order>>(x + this.contextToQuery(pageContext))),
        tap(x => this.logger.debug(x)),
        catchError(x => {
          this.logger.error(x);
          const data = new DataPage<Order>();
          data.count = 0;
          data.data = [];
          return of(data);
        })
      );
  }

  private contextToQuery(pageContext: PageContext): string {
    const parameters: string[] = [];
    parameters.push(`offset=${pageContext.offset}`);
    parameters.push(`length=${pageContext.pageSize}`);
    if (pageContext.orderColumn ) { parameters.push(`orderColumn=${pageContext.orderColumn}`); }
    if (pageContext.orderType ) {parameters.push(`orderType=${pageContext.orderType}`); }
    if (pageContext.filter.orderIdValue) { parameters.push(`orderId=${pageContext.filter.orderIdValue}`); }
    if (pageContext.filter.orderIdLimit) { parameters.push(`orderIdComparator=${pageContext.filter.orderIdLimit}`); }
    if (pageContext.filter.orderStatus) { parameters.push(`orderStatus=${pageContext.filter.orderStatus}`); }
    if (pageContext.filter.deliveryType) { parameters.push(`deliveryType=${pageContext.filter.deliveryType}`); }
    if (pageContext.filter.deliveryDateValue) { parameters.push(`deliveryDate=${pageContext.filter.deliveryDateValue}`); }
    if (pageContext.filter.deliveryDateLimit) { parameters.push(`deliveryDateComparator=${pageContext.filter.deliveryDateLimit}`); }
    if (pageContext.filter.deliveryTimeValue) { parameters.push(`deliveryTime=${pageContext.filter.deliveryTimeValue}`); }
    if (pageContext.filter.deliveryTimeLimit) { parameters.push(`deliveryTimeComparator=${pageContext.filter.deliveryTimeLimit}`); }
    if (pageContext.filter.customerName) { parameters.push(`customerName=${pageContext.filter.customerName}`); }
    if (pageContext.filter.deliveryAddress) { parameters.push(`deliveryAddress=${pageContext.filter.deliveryAddress}`); }
    if (pageContext.filter.contactPhone) { parameters.push(`contactPhone=${pageContext.filter.contactPhone}`); }
    if (pageContext.filter.price) { parameters.push(`price=${pageContext.filter.price}`); }
    if (pageContext.filter.paymentStatus) { parameters.push(`paymentStatus=${pageContext.filter.paymentStatus}`); }
    if (pageContext.restaurantId) { parameters.push(`restaurantId=${pageContext.restaurantId}`); }
    return '?' + parameters.join('&');
  }
}

export class PageContext {
  offset: number;
  pageSize: number;
  orderColumn: string;
  orderType: SortDirection;
  filter: OrderFilter;
  restaurantId: string;
  static defaultContext(restaurantId: string): PageContext {
    const context = new PageContext();
    context.offset = 0;
    context.pageSize = 20;
    context.orderColumn = null;
    context.orderType = SortDirection.desc;
    context.filter = {};
    context.restaurantId = restaurantId;
    return context;
  }
}
export interface OrderFilter {
  orderIdLimit?: string;
  orderIdValue?: string;
  orderStatus?: string;
  deliveryType?: string;
  deliveryDateLimit?: string;
  deliveryDateValue?: string;
  deliveryTimeLimit?: string;
  deliveryTimeValue?: string;
  customerName?: string;
  deliveryAddress?: string;
  contactPhone?: string;
  price?: string;
  paymentStatus?: string;
  restaurantName?: string;
}

export class DataPage<T> {
  count: number;
  data: T[];
}
