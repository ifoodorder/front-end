
export interface FlatOrder {
    id: string;
    restaurantId: string;
    orderNumber?: string;
    timestamp: Date;
    note?: string;
    contactPhone: string;
    orderStatus: string;
    address_id: string;
    address_country?: string;
    address_city: string;
    address_zipCode: string;
    address_street: string;
    address_building: string;
    orderSource: string;
    payment_paymentStatus: string;
    customer: string;
    orderItems: OrderItem[];
    paymentType?: string;
    delivery_id: string;
    delivery_deliveryType: string;
    delivery_deliveryDate: string;
    delivery_deliveryFrom: string;
    delivery_deliveryTo: string;
    orderPrice: string;
}
export interface Order {
    id: string;
    restaurantId: string;
    orderNumber?: string;
    timestamp: Date;
    note?: string;
    contactPhone: string;
    orderStatus: string; // TODO
    address: Address;
    orderSource: string;
    payment: Payment;
    delivery: Delivery;
    customer: string;
    orderItems: OrderItem[];
    paymentType?: string;
  }

export interface Address {
    id: string;
    country?: string;
    city: string;
    zipCode: string;
    street: string;
    building: string;
  }

export interface Payment {
    paymentStatus: string;
  }
export interface Delivery {
      id: string;
      deliveryType: string;
      deliveryDate: string;
      deliveryFrom: string;
      deliveryTo: string;
  }
export interface OrderItem {
      id: string;
      count: number;
      note: string;
      unitPrice: string;
      itemId: string;
  }
