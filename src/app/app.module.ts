import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbSidebarModule, NbStepperModule, NbCardModule, NbInputModule, NbTabsetModule, NbToggleModule, NbButtonGroupModule, NbIconModule, NbSelectModule, NbFormFieldModule, NbSpinnerModule, NbAutocompleteModule, NbDialogModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { DevComponent } from './dev/dev.component';
import { CustomerComponent } from './customer/customer.component';
import { CompanyComponent } from './company/company.component';
import { MenuComponent } from './company/menu/menu.component';
import { OrdersComponent } from './company/orders/orders.component';
import { RegistrationComponent } from './company/registration/registration.component';
import { SettingsComponent } from './company/settings/settings.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LoginComponent } from './company/login/login.component';
import { AutoCompleteInputComponent } from './components/auto-complete-input/auto-complete-input.component';
import { ExistingRestaurantWarningDialog } from './company/dialogs/existing-restaurant-warning-dialog.component';
import { EnvironmentService } from './environment.service';
import { map, mergeMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

function initializeKeycloak(keycloak: KeycloakService, environment: EnvironmentService): () => Subscription {
  
  return () => environment.config.pipe(
    map(x => x.ssoHost),
    mergeMap(x => initKeycloak(keycloak, x))
  ).subscribe();
}
function initKeycloak(keycloak: KeycloakService, ssoHost: string): Promise<boolean> {
  return keycloak.init({
    config: {
      url: ssoHost,
      realm: 'restaurants',
      clientId: 'Restaurant'
    },
    initOptions: {
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html'
    },
    enableBearerInterceptor: true
  });
}

@NgModule({
  declarations: [
    AppComponent,
    DevComponent,
    CustomerComponent,
    CompanyComponent,
    MenuComponent,
    OrdersComponent,
    RegistrationComponent,
    SettingsComponent,
    NotFoundComponent,
    LoginComponent,
    AutoCompleteInputComponent,
    ExistingRestaurantWarningDialog
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    KeycloakAngularModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.OFF
    }),
    NbDialogModule.forRoot({autoFocus: true}),
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,
    NbButtonModule,
    NbSidebarModule.forRoot(),
    NbStepperModule,
    NbCardModule,
    NbInputModule,
    NbTabsetModule,
    NbToggleModule,
    NbSelectModule,
    NbFormFieldModule,
    NbButtonGroupModule,
    NbSpinnerModule,
    NbAutocompleteModule,
    NbToastrModule.forRoot(),
    NgxDatatableModule
  ],
  providers: [{
    provide: APP_INITIALIZER,
    useFactory: initializeKeycloak,
    multi: true,
    deps: [KeycloakService, EnvironmentService]
  },
  {
    provide: 'externalUrlRedirectResolver',
    useValue: (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
      window.location.href = (route.data as any).externalUrl;
    }
  }],
  bootstrap: [AppComponent],
  exports: [
    CommonModule,
    TranslateModule
  ]
})
export class AppModule { }

