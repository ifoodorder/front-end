import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MenuBuilderService } from 'src/app/services/menu-builder.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  restaurantId: string;
  constructor(private fb: FormBuilder,
              private logger: NGXLogger,
              private service: MenuBuilderService,
              private activatedRoute: ActivatedRoute) { }

  get itemNames() {
    const items = new Set<string>();
    for (const category of this.categories) {
      const formGroup = category as FormGroup;
      const itemArr = formGroup?.controls?.items as FormArray;
      if (null == itemArr) {
        continue;
      }
      for (let i = 0; i < itemArr.length; i++) {
        const fGroup = itemArr.controls[i] as FormGroup;
        const value = fGroup.get('name').value;
        if (value !== '') {
          items.add(fGroup.get('name').value);
        }
      }
    }
    return items;
  }

  get categories() {
    const x = (this.menu.get('categories') as FormArray).controls;
    return x;
  }

  get categoryNames() {
    const categories = new Set<string>();
    const formArray = this.menu.get('categories') as FormArray;
    for (let i = 0; i < formArray.length; i++) {
      const fGroup = formArray.controls[i] as FormGroup;
      const name = fGroup.get('name').value;
      if (name !== '') {
        categories.add(name);
      }
    }
    return categories;
  }
  menuTitlePlaceholder = 'Pizzeria Valenzia';
  menuInfoPlaceholder = 'Last order is accepted 30 minutes before closing!';
  itemDescriptionPlaceholder = 'Very spicy!';
  itemImagePlaceholder = 'https://via.placeholder.com/150';

  menu: FormGroup = MenuBuilderModel.createFormGroup(this.fb);
  subscriptions: Subscription[] = [];

  alergens = [];
  private static itemToFormGroup(item: MenuItem, fb: FormBuilder) {
    return MenuItemModel.createFormGroup(fb, item);
  }
  ngOnInit(): void {

    this.logger.debug(this.activatedRoute.parent.snapshot);
    this.restaurantId = this.activatedRoute.parent.snapshot.paramMap.get('id');
    this.subscriptions.push(this.service.getPublicMenu(this.restaurantId)
                .pipe(map(x => MenuBuilderModel.createFormGroup(this.fb, x)))
                .subscribe(x => {
      this.menu = x;
    }));
    this.subscriptions.push(this.service.getAlergens(this.restaurantId)
      .subscribe(x => this.alergens = x)
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  createNewCategory() {
    const categories = this.menu.get('categories') as FormArray;
    categories.push(MenuCategoryModel.createFormGroup(this.fb));
  }

  createItem(categoryIndex: number) {
    const categories = this.menu.get('categories') as FormArray;
    const category = categories.controls[categoryIndex] as FormGroup;
    const items = category.get('items') as FormArray;
    items.push(MenuItemModel.createFormGroup(this.fb, null));
  }

  menuActivationToggle() {
    this.menu.get('enabled').setValue(!this.menu.get('enabled').value);
  }
  changeCategoryVisibility(categoryIndex: number) {
    const categories = this.menu.get('categories') as FormArray;
    const category = categories.controls[categoryIndex] as FormGroup;
    const enabled = category.get('enabled') as FormControl;
    enabled.setValue(!enabled.value);
  }

  deleteCategory(categoryIndex: number) {
    // const dialogRef = this.dialog.open(DeleteCategoryComponent);
    // this.subscriptions.push(dialogRef.afterClosed().subscribe(x => {
    //   if (x) {
    //     const categories = this.menu.get('categories') as FormArray;
    //     categories.controls.splice(categoryIndex, 1);
    //   }
    // }));
  }

  duplicateCategory(categoryIndex: number) {
    const categories = this.menu.get('categories') as FormArray;
    const category = categories.controls[categoryIndex] as FormGroup;
    const rawCategory = category.getRawValue();
    const newCategory = MenuCategoryModel.createFormGroup(this.fb, rawCategory);
    const name = newCategory.get('name');
    name.setValue(name.value + ' (copy)');
    categories.push(newCategory);
  }

  changeItemVisibility(categoryIndex: number, itemIndex: number) {
    const categories = this.menu.get('categories') as FormArray;
    const category = categories.controls[categoryIndex] as FormGroup;
    const items = category.get('items') as FormArray;
    const item = items.controls[itemIndex] as FormGroup;
    const enabled = item.get('enabled') as FormControl;
    enabled.setValue(!enabled.value);
  }

  changeItemImage(categoryIndex: number, itemIndex: number) {
    const input = document.createElement('input');
    input.style.visibility = 'hidden';
    input.type = 'file';
    input.accept = 'image/*';
    input.onchange = (e: Event) => {
      const file = (e.target as any).files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = readerEvent => {
        const contents = readerEvent.target.result;
        const categories = this.menu.get('categories') as FormArray;
        const category = categories.controls[categoryIndex] as FormGroup;
        const items = category.get('items') as FormArray;
        const item = items.controls[itemIndex] as FormGroup;
        const image = item.get('image');
        image.setValue(contents);
        input.remove();
      };
    };
    input.click();
  }

  deleteItem(categoryIndex: number, itemIndex: number) {
    // const dialogRef = this.dialog.open(DeleteItemComponent);
    // dialogRef.afterClosed().subscribe(x => {
    //   if (x) {
    //     const categories = this.menu.get('categories') as FormArray;
    //     const category = categories.controls[categoryIndex] as FormGroup;
    //     const items = category.get('items') as FormArray;
    //     items.controls.splice(itemIndex, 1);
    //   }
    // });
  }

  duplicateItem(categoryIndex: number, itemIndex: number) {
    const categories = this.menu.get('categories') as FormArray;
    const category = categories.controls[categoryIndex] as FormGroup;
    const items = category.get('items') as FormArray;
    const item = items.controls[itemIndex] as FormGroup;
    const itemRaw = item.getRawValue();
    const newItem = MenuComponent.itemToFormGroup(itemRaw, this.fb);
    const name = newItem.get('name');
    name.setValue(name.value + ' (copy)');
    items.controls.push(newItem);
  }

  copyItem() {

  }

  log(inp: any) {
    this.logger.debug(inp);
    return inp;
  }

  save() {
    const menu = this.menu.getRawValue() as Menu;
    this.service.saveMenu(menu, this.restaurantId).subscribe(x => {
      // const text = x.success ? 'Menu saved' : 'Failure during saving';

      // this.snackBar.open(text, null, {duration: 2000,
      //   verticalPosition: 'bottom',
      // });
    });
  }

  scroll(id) {
    document.getElementById(id).scrollIntoView({behavior: 'smooth'});
  }
}

export class MenuCategoryModel {
  public static createFormGroup(formBuilder: FormBuilder, currentMenuCategory?: MenuCategory): FormGroup  {
      const name = new FormControl('');
      const enabled = new FormControl(false);
      const items = new FormArray([]);
      if (currentMenuCategory != null) {
          name.setValue(currentMenuCategory.name);
          enabled.setValue(currentMenuCategory.enabled);
          currentMenuCategory.items.map(x => MenuItemModel.createFormGroup(formBuilder, x)).forEach(x => items.push(x));
      }
      return formBuilder.group({name, enabled, items});
  }
}

export class MenuItemModel {
  public static createFormGroup(formBuilder: FormBuilder, currentMenuItem?: MenuItem): FormGroup  {
      const name = new FormControl('');
      const enabled = new FormControl(false);
      const alergens = new FormControl([]);
      const price = new FormControl('');
      const description = new FormControl('');
      const image = new FormControl('');
      if (currentMenuItem != null) {
          name.setValue(currentMenuItem.name);
          enabled.setValue(currentMenuItem.enabled);
          alergens.setValue(currentMenuItem.alergens);
          price.setValue(currentMenuItem.price);
          description.setValue(currentMenuItem.description);
          image.setValue(currentMenuItem.image);
      }
      return formBuilder.group({name, enabled, alergens, price, description, image});
  }
}

export interface MenuItem {
  id: string;
  name: string;
  enabled: boolean;
  alergens: string[];
  price: string | number;
  description: string;
  image: string;
}

export interface MenuCategory {
  name: string;
  enabled: boolean;
  items: MenuItem[];
}

export interface Menu {
  title: string;
  description: string;
  categories: MenuCategory[];
  enabled: boolean;
}

export class MenuBuilderModel {
  public static createFormGroup(formBuilder: FormBuilder, currentMenuBuilder?: Menu): FormGroup {
      const title = new FormControl('');
      const description = new FormControl('');
      const categories = new FormArray([]);
      const enabled = new FormControl(false);

      if (currentMenuBuilder != null) {
          title.setValue(currentMenuBuilder.title);
          description.setValue(currentMenuBuilder.title);
          currentMenuBuilder
              .categories
              .map(x => MenuCategoryModel.createFormGroup(formBuilder, x))
              .forEach(x => categories.push(x));
          enabled.setValue(currentMenuBuilder.enabled);
      }
      return formBuilder.group({title, description, categories, enabled});
  }
}
