import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";

export type FromTo = {from: string, to: string};
export type ValueStatus = {val: string, status: any};
export type Week = {
    monday: FromTo[],
    tuesday: FromTo[]
    wednesday: FromTo[]
    thursday: FromTo[]
    friday: FromTo[]
    saturday: FromTo[]
    sunday: FromTo[]
};
export class RegistrationUtils {
    static generateFromAndToPair(formBuilder: FormBuilder, fromTo: FromTo = {from: '', to: ''}): FormGroup {
        return formBuilder.group({
          from: [fromTo.from],
          to: [fromTo.to]
        });
    }
    

    static generateFormDay(formBuilder: FormBuilder, fromTos: FromTo[] = []): FormArray {
        if (fromTos.length === 0) {
            return formBuilder.array([RegistrationUtils.generateFromAndToPair(formBuilder)], Validators.required);
        } else {
            return formBuilder.array([...fromTos.map(x => RegistrationUtils.generateFromAndToPair(formBuilder, x))]);
        }
    }

    static formatFromAndToPair(input: FromTo): ValueStatus {
        return {val: input.from + ' - ' + input.to, status: null};
    }

    static isClosedFilter(input: FromTo): boolean {
        if (input.from === '' || input.to === '') {
          return false;
        }
        return true;
    }

    static formatDays(day: FromTo[]): {openingHours: ValueStatus[], isClosed: boolean}  {
        const filteredDay = day.filter(RegistrationUtils.isClosedFilter);
        const isClosed = filteredDay.length === 0;
        const openingHours = filteredDay.map(RegistrationUtils.formatFromAndToPair);
        return {openingHours , isClosed};
    }
}