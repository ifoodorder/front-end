import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { FromTo, RegistrationUtils, Week } from './utils';
import { filter, mergeMap, tap } from 'rxjs/operators';
import { Restaurant } from 'src/app/services/restaurantApi';
import { KeycloakUserRoles, UserService } from 'src/app/services/user.service';
import { NbDialogService } from '@nebular/theme';
import { ExistingRestaurantWarningDialog } from '../dialogs/existing-restaurant-warning-dialog.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  isLoadaded = false;
  contactInfo = this.formBuilder.group({
    restaurantName: ['', Validators.required],
    restaurantPhone: ['', Validators.required],
    restaurantEmail: ['', [Validators.email, Validators.required]],
    country: ['', Validators.required],
    state: ['', Validators.required],
    city: ['', Validators.required],
    zip: ['', Validators.required],
    street: ['', Validators.required],
  });



  monday = RegistrationUtils.generateFormDay(this.formBuilder);
  tuesday = RegistrationUtils.generateFormDay(this.formBuilder);
  wednesday = RegistrationUtils.generateFormDay(this.formBuilder);
  thursday = RegistrationUtils.generateFormDay(this.formBuilder);
  friday = RegistrationUtils.generateFormDay(this.formBuilder);
  saturday = RegistrationUtils.generateFormDay(this.formBuilder);
  sunday = RegistrationUtils.generateFormDay(this.formBuilder);


  openingHours = this.formBuilder.group({
    monday: this.monday,
    tuesday: this.tuesday,
    wednesday: this.wednesday,
    thursday: this.thursday,
    friday: this.friday,
    saturday: this.saturday,
    sunday: this.sunday,
    info: ['']
  });

  private dayMap = new Map([
    ['monday', this.monday],
    ['tuesday', this.tuesday],
    ['wednesday', this.wednesday],
    ['thursday', this.thursday],
    ['friday', this.friday],
    ['saturday', this.saturday],
    ['sunday', this.sunday]
  ]);

  valueMap = new Map([
    ['monday', {translationId: 'DAY.MONDAY', control: this.monday}],
    ['tuesday', {translationId: 'DAY.TUESDAY', control: this.tuesday}],
    ['wednesday', {translationId: 'DAY.WEDNESDAY', control: this.wednesday}],
    ['thursday', {translationId: 'DAY.THURSDAY', control: this.thursday}],
    ['friday', {translationId: 'DAY.FRIDAY', control: this.friday}],
    ['saturday', {translationId: 'DAY.SATURDAY', control: this.saturday}],
    ['sunday', {translationId: 'DAY.SUNDAY', control: this.sunday}]
  ]);
  days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

  registrationRedirectObserver = () => {
    this.logger.debug('Redirecting to registration');
    this.userService.register(location.href);
  }
  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private router: Router,
              private restaurantService: RestaurantService,
              private logger: NGXLogger,
              private dialogService: NbDialogService) { }

  ngOnInit(): void {
    this.userService.isLoggedIn().pipe(
      filter(x => !x)
    ).subscribe(this.registrationRedirectObserver);
    this.userService.isLoggedIn().pipe(
      filter(x => x),
      mergeMap(() => this.userService.addRole(KeycloakUserRoles.RESTAURANT_ADMIN)),
      mergeMap(() => this.userService.createGroup()),
      mergeMap(() => this.userService.refreshToken())
    ).subscribe(() => {
      this.logger.debug('Created group and added restaurant_admin role');
      this.isLoadaded = true;
    });

    this.restaurantService.getRestaurants({owningOnly: true}).pipe(
      filter(x => x.length > 0)
    ).subscribe(x => {
      this.dialogService.open(ExistingRestaurantWarningDialog, {context: {restaurants: x}, closeOnBackdropClick: false, closeOnEsc: false});
    });
  }

  get contactInfoValues() {
    return this.contactInfo.getRawValue();
  }


  get openingHoursValues() {
    const values = this.openingHours.getRawValue() as Week;
    return {
      monday: RegistrationUtils.formatDays(values.monday),
      tuesday: RegistrationUtils.formatDays(values.tuesday),
      wednesday: RegistrationUtils.formatDays(values.wednesday),
      thursday: RegistrationUtils.formatDays(values.thursday),
      friday: RegistrationUtils.formatDays(values.friday),
      saturday: RegistrationUtils.formatDays(values.saturday),
      sunday: RegistrationUtils.formatDays(values.sunday),
    };
  }

  isOpened(day: string): boolean {
    return this.openingHoursValues[day].isClosed;
  }

  getOpeningHours(day: string): string[] {
    return this.openingHoursValues[day].openingHours;
  }

  addRange(day: string): void {
    const arr = this.dayMap.get(day) as FormArray;
    arr.push(RegistrationUtils.generateFromAndToPair(this.formBuilder));
  }
  createEmptyRestaurant(): void {
    this.restaurantService.postRestaurant({}).pipe(
      tap(() => this.logger.debug('created empty restaurant'))
    ).subscribe(restaurant => {
      this.logger.debug('Redirecting to restaurant', restaurant?.id);
      this.router.navigate([`/company/${restaurant.id}/${restaurant.name}`]);
    });
  }
  createRestaurant(): void {
    this.logger.info("monday: ", this.dayMap.get('monday').getRawValue());
    const restaurantObject: Restaurant = {
      name: this.contactInfoValues.restaurantName,
      address: {
        country: this.contactInfoValues.country,
        // TODO: state is missing
        city: this.contactInfoValues.city,
        zip: this.contactInfoValues.zip,
        street: this.contactInfoValues.street
      },
      currency: null, // TODO: ask directly or inferr from language
      visible: false,
      deliverySettings: {},
      logo: null,
      note: null,
      paymentSettings: {},
      openingHoursSettings: {
        note: this.openingHours.getRawValue().info,
        openingHours: [
          ...(this.dayMap.get('monday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'MO'})),
          ...(this.dayMap.get('tuesday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'TU'})),
          ...(this.dayMap.get('wednesday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'WE'})),
          ...(this.dayMap.get('thursday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'TH'})),
          ...(this.dayMap.get('friday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'FR'})),
          ...(this.dayMap.get('saturday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'SA'})),
          ...(this.dayMap.get('sunday').getRawValue() as FromTo[]).filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'SU'})),
        ]
      }
    };
    this.restaurantService.postRestaurant(restaurantObject).subscribe(restaurant => {
      this.logger.debug('Redirecting to restaurant', restaurant?.id);
      this.router.navigate([`/company/${restaurant.id}/${restaurant.name}`]);
    });
  }
}
