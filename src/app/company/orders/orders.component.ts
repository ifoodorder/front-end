import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FlatOrder, Order, OrderItem } from 'src/app/services/order';
import { OrderService, PageContext } from 'src/app/services/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  restaurantId: string;
  rows = [];
  isLoading = false;
  count = 0;

  constructor(private orderService: OrderService,
              private activatedRoute: ActivatedRoute) {
    this.restaurantId = this.activatedRoute.parent.snapshot.params.id;
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.orderService.findOrdersLimitAndOrderBy(PageContext.defaultContext(this.restaurantId)).subscribe(x => {
      this.rows = x.data.map(this.flattenOrder);
      this.count = x.count;
      this.isLoading = false;
    });
  }

  private flattenOrder(order: Order): FlatOrder {
    return {
      address_building: order.address.building,
      address_city: order.address.city,
      address_id: order.address.id,
      address_street: order.address.street,
      address_zipCode: order.address.zipCode,
      contactPhone: order.contactPhone,
      customer: order.customer,
      id: order.id,
      orderItems: order.orderItems,
      orderSource: order.orderSource,
      orderStatus: order.orderSource,
      orderNumber: order.orderNumber,
      payment_paymentStatus: order.payment.paymentStatus,
      restaurantId: order.restaurantId,
      timestamp: order.timestamp,
      address_country: order.address.country,
      note: order.note,
      paymentType: order.paymentType,
      delivery_deliveryDate: order.delivery.deliveryDate,
      delivery_deliveryFrom: order.delivery.deliveryFrom,
      delivery_deliveryTo: order.delivery.deliveryTo,
      delivery_deliveryType: order.delivery.deliveryType,
      delivery_id: order.delivery.id,
      orderPrice: order.orderItems.map(x => x.unitPrice as unknown as number).reduce((x, y) => x + y).toString()
    };
  }

}
