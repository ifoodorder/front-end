import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NbDialogRef } from '@nebular/theme';
import { NGXLogger } from 'ngx-logger';
import { Restaurant } from 'src/app/services/restaurantApi';

@Component({
  selector: 'app-existing-restaurant-warning-dialog',
  templateUrl: './existing-restaurant-warning-dialog.component.html'
})
export class ExistingRestaurantWarningDialog implements OnInit {
  @Input() restaurants: Restaurant[];
  constructor(private dialogRef: NbDialogRef<ExistingRestaurantWarningDialog>,
              private router: Router,
              private logger: NGXLogger) { }

  ngOnInit(): void {
    this.logger.info('Dialog ref:', this.dialogRef);
  }

  createNew(): void {
    this.logger.info('closing dialog');
    this.dialogRef.close();
  }

  continueToExisting(): void {
    this.logger.info('closing dialog');
    this.router.navigate([`/company/${this.restaurants[0].id}/${this.restaurants[0].name}`]);
    this.dialogRef.close();
  }

}
