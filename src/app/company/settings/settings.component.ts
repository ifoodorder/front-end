import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { NGXLogger } from 'ngx-logger';
import { Observable, of } from 'rxjs';
import { map, mergeAll } from 'rxjs/operators';
import { TranslatableOption } from 'src/app/components/auto-complete-input/auto-complete-input.component';
import { RestaurantService } from 'src/app/services/restaurant.service';
import { Restaurant } from 'src/app/services/restaurantApi';
import { RegistrationUtils } from '../registration/utils';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  private restaurantId: string;
  filteredCountries$: Observable<string[]>;
  countryOptions: Observable<TranslatableOption>;
  @ViewChild('countryInput') countryInput;

  buttons = [
    {value: 'CASH', disabled: false},
    {value: 'CARD_ONLINE', disabled: true},
    {value: 'CARD_COURIER', disabled: false},
    {value: 'BANK_TRANSFER', disabled: false},
    {value: 'MEAL_VOUCHER', disabled: false},
    {value: 'BITCOIN', disabled: true}
  ];

  deliveryButtons = [
    {value: 'RESTAURANT', disabled: false},
    {value: 'COURIER', disabled: false},
    {value: 'PICKUP_POINT', disabled: false}
  ];

  contactInfo = this.formBuilder.group({
    restaurantName: ['', Validators.required],
    street: ['', Validators.required],
    zip: ['', Validators.required],
    city: ['', Validators.required],
    country: ['', Validators.required],
    note: ['', Validators.required],
    visible: [{value: false, disabled: true}, Validators.required]
  });

  legalInfo = this.formBuilder.group({
    companyName: ['', Validators.required],
    street: ['', Validators.required],
    zip: ['', Validators.required],
    city: ['', Validators.required],
    entityNumber: ['', Validators.required],
    taxId: ['', Validators.required]
  });

  paymentsSettigns = this.formBuilder.group({
    paymentOptions: [[], Validators.required],
    bankTransferAccount: ['', Validators.required],
    bankTransferNote: ['', Validators.required],
    cashNote: ['', Validators.required],
    cardCourierNote: ['', Validators.required],
    mealVoucherNote: ['', Validators.required]
  });

  deliverySettings = this.formBuilder.group({
    deliveryOptions: [[], Validators.required],
    restaurant: ['', Validators.required],
    restaurantMinimumAmount: ['', Validators.required],
    restaurantFee: ['', Validators.required], // TODO: increase customization
    courier: ['', Validators.required],
    courierMinimumAmount: ['', Validators.required],
    courierFee: ['', Validators.required], // TODO: increase customization
    courierArea: [''],
    courierNote: ['', Validators.required],
    pickupPoint: ['', Validators.required],
    pickupPointMinimumAmount: ['', Validators.required],
    pickupPointFee: ['', Validators.required], // TODO: increase customization
  });

  monday = RegistrationUtils.generateFormDay(this.formBuilder);
  tuesday = RegistrationUtils.generateFormDay(this.formBuilder);
  wednesday = RegistrationUtils.generateFormDay(this.formBuilder);
  thursday = RegistrationUtils.generateFormDay(this.formBuilder);
  friday = RegistrationUtils.generateFormDay(this.formBuilder);
  saturday = RegistrationUtils.generateFormDay(this.formBuilder);
  sunday = RegistrationUtils.generateFormDay(this.formBuilder);


  openingHours = this.formBuilder.group({
    monday: this.monday,
    tuesday: this.tuesday,
    wednesday: this.wednesday,
    thursday: this.thursday,
    friday: this.friday,
    saturday: this.saturday,
    sunday: this.sunday,
    note: ['']
  });

  get isBankTransfer(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('BANK_TRANSFER');
  }

  get isCash(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('CASH');
  }

  get isCardToCourier(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('CARD_COURIER');
  }

  get isCardOnline(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('CARD_ONLINE');
  }

  get isMealVoucher(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('MEAL_VOUCHER');
  }

  get isBitcoin(): boolean {
    const current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    return current.includes('BITCOIN');
  }

  get isRestaurant(): boolean {
    const current = this.deliverySettings.controls.deliveryOptions.value as string[];    
    return current.includes('RESTAURANT');
  }
  get isCourier(): boolean {
    const current = this.deliverySettings.controls.deliveryOptions.value as string[];
    return current.includes('COURIER');
  }
  get isPickupPoint(): boolean {
    const current = this.deliverySettings.controls.deliveryOptions.value as string[];
    return current.includes('PICKUP_POINT');
  }

  private dayMap = new Map([
    ['monday', this.monday],
    ['tuesday', this.tuesday],
    ['wednesday', this.wednesday],
    ['thursday', this.thursday],
    ['friday', this.friday],
    ['saturday', this.saturday],
    ['sunday', this.sunday]
  ]);

  valueMap = new Map([
    ['monday', {translationId: 'DAY.MONDAY', control: this.monday}],
    ['tuesday', {translationId: 'DAY.TUESDAY', control: this.tuesday}],
    ['wednesday', {translationId: 'DAY.WEDNESDAY', control: this.wednesday}],
    ['thursday', {translationId: 'DAY.THURSDAY', control: this.thursday}],
    ['friday', {translationId: 'DAY.FRIDAY', control: this.friday}],
    ['saturday', {translationId: 'DAY.SATURDAY', control: this.saturday}],
    ['sunday', {translationId: 'DAY.SUNDAY', control: this.sunday}]
  ]);
  days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
  
  constructor(private formBuilder: FormBuilder,
              private restaurantService: RestaurantService,
              private activatedRoute: ActivatedRoute,
              private logger: NGXLogger,
              private toesterService: NbToastrService) {
      
    this.restaurantId = this.activatedRoute.parent.snapshot.params.id;
  }

  ngOnInit(): void {
    this.restaurantService.getRestaurantById(this.restaurantId).subscribe(x => {
      this.contactInfo  = this.formBuilder.group({
        restaurantName: [x?.name, Validators.required],
        street: [x?.address?.street, Validators.required],
        zip: [x?.address?.zip, Validators.required],
        city: [x?.address?.city, Validators.required],
        country: [x?.address?.country, Validators.required],
        note: [x?.note, Validators.required]
      });
      const paymentOptions = [];
      if(x?.paymentSettings?.bankTransfer?.enabled) { paymentOptions.push('BANK_TRANSFER'); }
      if(x?.paymentSettings?.cash?.enabled) { paymentOptions.push('CASH'); }
      if(x?.paymentSettings?.bitcoin?.enabled) { paymentOptions.push('BITCOIN'); }
      if(x?.paymentSettings?.cardOnDelivery?.enabled) { paymentOptions.push('CARD_COURIER'); }
      if(x?.paymentSettings?.cardOnline?.enabled) { paymentOptions.push('CARD_ONLINE'); }
      if(x?.paymentSettings?.mealVoucher?.enabled) { paymentOptions.push('MEAL_VOUCHER'); }

      this.paymentsSettigns = this.formBuilder.group({
        paymentOptions: [paymentOptions, Validators.required],
        bankTransferAccount: [x?.paymentSettings?.bankTransfer?.bankAccountNumber || '', Validators.required],
        bankTransferNote: ['', Validators.required],
        cashNote: ['', Validators.required],
        cardCourierNote: ['', Validators.required],
        mealVoucherNote: ['', Validators.required]
      })
      const mapTo = (restaurant: Restaurant, day: string) => {
        const empty = [{from: '', to: ''}];
        if (restaurant.openingHoursSettings == null) {
          return empty;
        }
        const openingHoursInDay = restaurant.openingHoursSettings.openingHours.filter(y => y?.day === day);
        if (openingHoursInDay.length === 0) {
          return empty;
        }
        const ret = openingHoursInDay.map(y => ({from: y?.from, to: y?.to}));
        this.logger.debug('mapTo', restaurant, ret, openingHoursInDay, day);
        return ret;
      };
      this.monday.setValue(mapTo(x, 'MO'));
      this.tuesday.setValue(mapTo(x, 'TU'));
      this.wednesday.setValue(mapTo(x, 'WE'));
      this.thursday.setValue(mapTo(x, 'TH'));
      this.friday.setValue(mapTo(x, 'FR'));
      this.saturday.setValue(mapTo(x, 'SA'));
      this.sunday.setValue(mapTo(x, 'SU'));

      this.openingHours = this.formBuilder.group({
        monday: this.monday,
        tuesday: this.tuesday,
        wednesday: this.wednesday,
        thursday: this.thursday,
        friday: this.friday,
        saturday: this.saturday,
        sunday: this.sunday,
        note: [x?.note]
      });
    });
    this.countryOptions = this.restaurantService.getCountries().pipe(
      mergeAll(),
      map(x => ({key: x, translationKey: 'COUNTRY.' + x})));
  }

  addRange(day: string): void {
    const arr = this.dayMap.get(day) as FormArray;
    arr.push(RegistrationUtils.generateFromAndToPair(this.formBuilder));
  }

  selectPaymentMethod(buttonName, event): void {
    let current = this.paymentsSettigns.controls.paymentOptions.value as string[];
    if (event) {
      current.push(buttonName);
    } else {
      current = current.filter(x => x !== buttonName);
    }
    this.paymentsSettigns.controls.paymentOptions.setValue(current);
    this.logger.debug(this.paymentsSettigns.controls.paymentOptions.value);
  }

  checkPaymentMethod(buttonName): boolean {
    return (this.paymentsSettigns.controls.paymentOptions.value as string[]).includes(buttonName);
  }

  selectDeliveryMethod(buttonName, event): void {
    let current = this.deliverySettings.controls.deliveryOptions.value as string[];
    if (event) {
      current.push(buttonName);
    } else {
      current = current.filter(x => x !== buttonName);
    }
    this.deliverySettings.controls.deliveryOptions.setValue(current);
    this.logger.debug(this.deliverySettings.controls.deliveryOptions.value);
  }

  save(): void {
    const paymentsSettigns = this.paymentsSettigns.getRawValue();
    const restaurantSettings = this.contactInfo.getRawValue();
    const openingHours = this.openingHours.getRawValue();
    this.logger.debug(paymentsSettigns);
    this.logger.debug(this.contactInfo.getRawValue());
    this.logger.debug(this.openingHours.getRawValue());
    this.logger.debug(this.legalInfo.getRawValue());

    const restaurant: Restaurant = {
      address: {
        building: null,
        city: restaurantSettings.city,
        country: restaurantSettings.country,
        street: restaurantSettings.street,
        zip: restaurantSettings.zip
      },
      name: restaurantSettings.restaurantName,
      currency: null,
      deliverySettings: {
        courierDelivery: null,
        pickupPointDelivery: null,
        restaurantDelivery: null
      },
      id: this.restaurantId,
      logo: null,
      note: restaurantSettings.note,
      openingHoursSettings: {
        note: openingHours.note,
        openingHours: [
          ...(openingHours.monday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'MO'}))),
          ...(openingHours.tuesday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'TU'}))),
          ...(openingHours.wednesday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'WE'}))),
          ...(openingHours.thursday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'TH'}))),
          ...(openingHours.friday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'FR'}))),
          ...(openingHours.saturday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'SA'}))),
          ...(openingHours.sunday.filter(x => !!x.from && !!x.to).map(x => ({from: x.from, to: x.to, day: 'SU'})))
        ]
      },
      paymentSettings: {
        bankTransfer: {
          id: null,
          additionalInfo: null,
          allowUnregistered: true,
          bankAccountNumber: paymentsSettigns.bankTransferAccount || '',
          enabled: (paymentsSettigns.paymentOptions as string[]).includes('BANK_TRANSFER'),
        },
        bitcoin: {
          enabled: (paymentsSettigns.paymentOptions as string[]).includes('BITCOIN'),
          address: '',
          id: null
        },
        cardOnDelivery: {
          allowUnregistered: true,
          cards: [],
          enabled: (paymentsSettigns.paymentOptions as string[]).includes('CARD_COURIER'),
          id: null
        },
        cardOnline: {
          enabled: (paymentsSettigns.paymentOptions as string[]).includes('CARD_ONLINE'),
          id: null
        },
        cash: {
          enabled: (paymentsSettigns.paymentOptions as string[]).includes('CASH'),
          allowUnregistered: true,
          currencies: [],
          id: null
        },
        mealVoucher: null
      },
      visible: restaurantSettings.visible
    };
    this.restaurantService.postRestaurant(restaurant).subscribe(() => {
      this.toesterService.success('Saved!', 'Settings');
    });
  }
}
