import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { first, mergeAll } from 'rxjs/operators';
import { RestaurantService } from 'src/app/services/restaurant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private sub: Subscription;
  constructor(private restaurantService: RestaurantService,
              private router: Router) { }
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  ngOnInit(): void {
    this.sub = this.restaurantService.getRestaurants({owningOnly: true}).pipe(
      mergeAll(),
      first())
    .subscribe(x => {
      this.router.navigate(['/company', x.id, x.name || 'restaurant']);
    });
  }

}
