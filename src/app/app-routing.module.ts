import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CompanyComponent } from './company/company.component';
import { LoginComponent } from './company/login/login.component';
import { MenuComponent } from './company/menu/menu.component';
import { OrdersComponent } from './company/orders/orders.component';
import { RegistrationComponent } from './company/registration/registration.component';
import { SettingsComponent } from './company/settings/settings.component';
import { CustomerComponent } from './customer/customer.component';
import { DevComponent } from './dev/dev.component';
import { AuthGuard } from './guards/auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  {component: CustomerComponent, path: 'customer', children: [
    // TODO
  ]},
  {component: LoginComponent, path: 'company/login', canActivate: [AuthGuard]},
  {component: RegistrationComponent, path: 'company/registration', canActivate: [AuthGuard]},
  {component: CompanyComponent, path: 'company/:id/:restaurantName', canActivate: [AuthGuard], children: [
    {path: '', pathMatch: 'full', redirectTo: 'orders'},
    {component: OrdersComponent, path: 'orders'},
    {component: MenuComponent, path: 'menu'},
    {component: SettingsComponent, path: 'settings'},
  ]},
  {component: DevComponent, path: 'dev'},
  {path: '', pathMatch: 'full', redirectTo: '/dev'},
  {component: NotFoundComponent, path: '**'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
