import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormBuilder, FormGroup, NG_VALIDATORS, NG_VALUE_ACCESSOR, ValidationErrors, Validator } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { NGXLogger } from 'ngx-logger';
import { merge, Observable, of, Subscription } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';

export type TranslatableOption = {key: string, translationKey: string};
export type TranslatedOption = {key: string, translation: string};
@Component({
  selector: 'app-auto-complete-input',
  templateUrl: './auto-complete-input.component.html',
  styleUrls: ['./auto-complete-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: AutoCompleteInputComponent
    }
  ]
})
export class AutoCompleteInputComponent implements OnInit, OnDestroy, ControlValueAccessor {

  @Input() placeholder: string;
  @Input() options: Observable<TranslatableOption>;
  @Input() class: string;

  form = this.fb.group({
    formInput: ['']
  });
  keyMap: Map<string, string> = new Map();
  translationMap: Map<string, string> = new Map();
  options$: string[] = [];
  filteredOptions$: Observable<string[]>;
  private writeValue$: any = null;
  private onChangeSubs: Subscription[] = [];
  private touched$ = false;
  onTouched = () => {};

  constructor(private logger: NGXLogger, private translate: TranslateService, private fb: FormBuilder) { }


  writeValue(obj: any): void {
    if (this.keyMap.size === 0) {
      this.writeValue$ = obj;
    } else {
      this.form.controls.formInput.setValue(this.keyMap.get(obj), {onlySelf: false, emitEvent: false});
    }
  }
  registerOnChange(fn: any): void {
    const sub = this.form.valueChanges.pipe(
      map(x => this.translationMap.get(x.formInput))).subscribe(fn);
    this.onChangeSubs.push(sub);
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    isDisabled ? this.form.disable() : this.form.enable();
  }

  ngOnInit(): void {
    this.options.pipe(mergeMap(x => {
      return this.translate.get(x.translationKey).pipe(map(y => ({key: x.key, translation: y})));
    })).subscribe(x => {
      this.translationMap.set(x.translation, x.key);
      this.keyMap.set(x.key, x.translation);
      this.options$.push(x.translation);
      this.filteredOptions$ = of(this.options$);
      if (this.writeValue$) {
        this.form.controls.formInput.setValue(this.keyMap.get(this.writeValue$), {onlySelf: false, emitEvent: false});
      }
    });
  }

  ngOnDestroy(): void {
    this.onChangeSubs.forEach(x => x.unsubscribe());
  }

  private filter(value: string): string[] {
    this.logger.debug(value);
    const filterValue = value.toLowerCase();
    return this.options$.filter(optionValue => optionValue.toLowerCase().includes(filterValue));
  }

  getFilteredOptions(value: string): Observable<string[]> {
    return of(value).pipe(
      map(filterString => this.filter(filterString)),
    );
  }

  onChange(): void {
    this.filteredOptions$ = this.getFilteredOptions(this.form.controls.formInput.value);
  }

  onSelectionChange($event): void {
    this.filteredOptions$ = this.getFilteredOptions($event);
  }

}
