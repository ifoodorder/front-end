import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ListItem, SidebarService } from './services/sidebar.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'iFoodOrder';
  private available = ['en', 'cs', 'sk', 'ro'];
  constructor(private translate: TranslateService,
              private sidebarService: SidebarService) {
    this.translate.addLangs(this.available);
    this.translate.setDefaultLang(this.available[0]);

    const lang = localStorage.getItem('lang');
    if (lang != null) {
      this.translate.use(lang);
    } else {
      const prefferedLanguages = navigator.languages.map(x => x.slice(0, 2));
      const firstMatchingLanguage = prefferedLanguages.find(x => this.available.includes(x)) || this.available[0];
      this.translate.use(firstMatchingLanguage);
      localStorage.setItem('lang', firstMatchingLanguage);
    }
  }

  get isSidebarVisible(): boolean {
    return this.sidebarService.isVisible();
  }

  get items(): ListItem[] {
    return this.sidebarService.getList();
  }
}
