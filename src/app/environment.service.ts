import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, ReplaySubject, Subject } from 'rxjs';
import { IEnvironment } from './services/environment';

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {
  private environment: ReplaySubject<IEnvironment> = new ReplaySubject();
  private static readonly configUrl = '/assets/environment/config.json';
  constructor(private http: HttpClient) {
    this.load();
  }
  private load(): void {
    this.http.get<IEnvironment>(EnvironmentService.configUrl).subscribe((x) => {
      this.environment.next(x);
    });
  }
  get config(): Observable<IEnvironment> {
    return this.environment.asObservable();
  }
}
