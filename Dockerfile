FROM alpine:3.13
RUN addgroup -S ifoodorder \
    && adduser -S ifoodorder -G ifoodorder \
    && apk update \
    && apk add nginx openssl bash \
    && mkdir /etc/nginx/templates/ \
    && mkdir -p /usr/share/nginx/html/front-end \
    && mkdir /run/nginx

COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/service.conf.tpl /etc/nginx/templates/service.conf.tpl
COPY docker/docker-entrypoint.sh /docker-entrypoint.sh
COPY dist/* /usr/share/nginx/html/front-end

RUN chmod +x /docker-entrypoint.sh \
    && chown ifoodorder:ifoodorder /docker-entrypoint.sh \
    && chown -R ifoodorder:ifoodorder /etc/nginx \
    && chown -R ifoodorder:ifoodorder /var/lib/nginx \
    && chown -R ifoodorder:ifoodorder /var/log/nginx \
    && chown -R ifoodorder:ifoodorder /run/nginx \
    && chown -R ifoodorder:ifoodorder /usr/share/nginx/html/front-end

USER ifoodorder:ifoodorder
ENV HTTP_PORT 80
ENV HTTPS_PORT 443

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/docker-entrypoint.sh"]