{
    "production": true,
    "menuHost": "{{ key "config/frontend/menuHost" }}",
    "orderHost": "{{ key "config/frontend/orderHost" }}",
    "restaurantHost": "{{ key "config/frontend/restaurantHost" }}",
    "userHost": "{{ key "config/frontend/userHost" }}",
    "ssoHost": "{{ key "config/frontend/ssoHost" }}",
    "nominatimHost": "{{ key "config/frontend/nominatimHost" }}",
    "nominatimKey": "{{ with secret "secret/frontend" }}{{ .Data.nominatimKey }}{{ end }}",
    "iconRedirectUrl": "{{ key "config/frontend/iconRedirectUrl" }}"
}