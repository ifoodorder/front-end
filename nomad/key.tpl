{{ with $ip_address := (env "NOMAD_HOST_IP_http") }}
{{ with secret "pki_int/issue/cert" "role_name=frontend" "common_name=frontend.service.consul" "ttl=24h" "alt_names=_frontend._tcp.service.consul,localhost" (printf "ip_sans=127.0.0.1,%s" $ip_address) }}
{{ .Data.private_key }}
{{ end }}{{ end }}